PROJECT_DIR = /home/mazura/Documents/www/APPS/trunk/backend/ssp/api/
DOCKER_SUBNET = 172.123.0.1/16
DOCKER_START = sudo dockerd --bip=$(DOCKER_SUBNET) &> dockerd-logfile &

start:
	$(DOCKER_START)
	sudo docker-compose up --build
	
stop:
	sudo docker-compose down
	
init:
	$(DOCKER_START)
	sudo docker run --rm --interactive --tty \
  --volume $(PROJECT_DIR):/app \
  composer update
	
clean:
	sudo docker container prune

doctrine_import_db:
	$(DOCKER_START)
	sudo docker build -t doctrine -f ./Dockerfile.doctrine . 
	sudo docker run --rm -v $(PROJECT_DIR):/app -w /app doctrine php vendor/bin/doctrine orm:convert-mapping -f --from-database --namespace='' yaml mapping/yaml
	sudo docker run --rm -v $(PROJECT_DIR):/app -w /app doctrine php vendor/bin/doctrine orm:generate-entities --generate-annotations=GENERATE-ANNOTATIONS --regenerate-entities=REGENERATE-ENTITIES -- ./src/Entities

